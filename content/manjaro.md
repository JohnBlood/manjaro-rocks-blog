+++
title = "What is Manjaro?"
draft = false
description = "What the heck is Manjaro?"
author = "John Paul Wohlscheid"
tags = [
  "manjaro",
  "sonar",
]
categories = [
  "about",
  "derivatives",
]
date = "2016-11-30T23:25:16-05:00"

+++

[Manjaro Linux](https://manjaro.github.io/) is a user-friendly GNU/Linux distribution based on the independently developed Arch Linux. Within the Linux community, Arch itself is renowned for being an exceptionally fast, powerful and lightweight distribution that provides access to the very latest cutting-edge software.

However, Arch is also traditionally aimed at more experienced or technically-minded users. As such, it is generally considered to be beyond the reach of many, especially those who lack the technical expertise (or persistence) required to use it.

Developed in Austria, France, and Germany, Manjaro aims to provide all of the benefits of Arch Linux combined with a focus on user-friendliness and accessibility. Available in both 32- and 64-bit versions, Manjaro is suitable for newcomers as well as experienced Linux users.

For newcomers, a user-friendly installer is provided, and the system itself is designed to work fully “straight out of the box” with features including:

* Pre-installed desktop environments
* Pre-installed graphical applications to easily install software and update your system
* Pre-installed codecs to play multimedia files,
* Pre-installed access to the latest games
